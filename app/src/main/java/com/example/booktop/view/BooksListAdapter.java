package com.example.booktop.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.booktop.R;
import com.example.booktop.model.bookModels.Item;

import java.util.List;

public class BooksListAdapter extends BaseAdapter {
    Context mContext ;
    List<Item> items;


    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return   items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
            View v = LayoutInflater.from(mContext)
                    .inflate(R.layout.books_list_item ,parent,false);
            Item item = items.get(position);

        ImageView imgItemBook=v.findViewById(R.id.imgItemBook);
        TextView bookTitle= v.findViewById(R.id.bookTitle);
        TextView bookAuthors= v.findViewById(R.id.bookAuthors);
        TextView DesCription= v.findViewById(R.id.bookDescription);

        bookTitle.setText(item.getVolumeInfo().getTitle());
        bookAuthors.setText(item.getVolumeInfo().getPublisher());
        DesCription.setText(item.getVolumeInfo().getDescription());

        Glide.with(mContext)
                .load(item.getVolumeInfo().getImageLinks().getThumbnail())
                .into(imgItemBook);

        return v;
    }
}
