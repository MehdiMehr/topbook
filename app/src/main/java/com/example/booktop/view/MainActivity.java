package com.example.booktop.view;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Adapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.booktop.MVPContract;
import com.example.booktop.R;
import com.example.booktop.model.bookModels.Item;
import com.example.booktop.presenter.MVPPresenter;

import java.util.List;

public class MainActivity extends AppCompatActivity implements MVPContract.View {
    ImageView imgBookID;
    EditText bookName;
    TextView textTV;
    MVPContract.Presenter presenter = new MVPPresenter();
    ProgressDialog dialog ;
    ListView bookItemsView;
    Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        presenter.attachView(this);
        dialog = new ProgressDialog(this);
        dialog.setTitle("Loading");
        dialog.setMessage("Please Wait to Load data from server");

        Bind();
    }

    private void Bind() {

        imgBookID = findViewById(R.id.imgBookID);
        bookName = findViewById(R.id.getURL);
        textTV=findViewById(R.id.textTV);

        bookItemsView=findViewById(R.id.bookItemsView);
        findViewById(R.id.btnSearch).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.onGetBook(bookName.getText().toString());
                textTV.setText(bookName.getText().toString()+" Results");
                dialog.show();
            }
        });



    }

    @Override
    public void onResultReceived(List<Item> bookItems) {
//        BooksListAdapter adapter = new BooksListAdapter(this ,bookItems);

        Toast.makeText(this, "Result is :"+bookItems, Toast.LENGTH_LONG).show();
        dialog.dismiss();

    }
}
