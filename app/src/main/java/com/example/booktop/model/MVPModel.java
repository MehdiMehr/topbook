package com.example.booktop.model;

import android.text.Editable;

import com.example.booktop.MVPContract;
import com.example.booktop.model.bookModels.Book;
import com.example.booktop.model.bookModels.Item;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import java.util.List;

import cz.msebera.android.httpclient.Header;

public class MVPModel implements MVPContract.Model {
MVPContract.Presenter presenter;
String result="" ;





    @Override
    public void onGetBook(String bookName) {

        String url ="https://www.googleapis.com/books/v1/volumes?q="+ bookName;

        AsyncHttpClient client = new AsyncHttpClient();
        client.get(url, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                result="Error in connection";

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                result=" Okay";
                parseAndShow(responseString);

            }

        });
    }


    @Override
    public void attachPresenter(MVPContract.Presenter presenter) {
    this.presenter=presenter;
    }

    @Override
    public void parseAndShow(String responseString) {
        Gson gson = new Gson();
        Book bookList = gson.fromJson(responseString,Book.class);
            String kind =bookList.getKind();

        List<Item> bookItems=
                bookList.getItems();

//        presenter.onResultReceived(bookList.getKind());
        presenter.onResultReceived(bookItems);


    }
}
