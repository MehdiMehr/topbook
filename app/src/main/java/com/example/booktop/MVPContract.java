package com.example.booktop;


import com.example.booktop.model.bookModels.Item;

import java.util.List;

public interface MVPContract {

    interface View {
        void onResultReceived(List<Item>  bookItems);



    }
    interface Presenter {

        void attachView (View view);
        void onGetBook(String name);
        void onResultReceived(List<Item> bookItems);
    }


    interface  Model{
        void onGetBook(String bookName);
        void attachPresenter (Presenter presenter);
        void parseAndShow(String responseString);





    }





}
