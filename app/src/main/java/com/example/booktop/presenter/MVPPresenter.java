package com.example.booktop.presenter;

import android.text.Editable;

import com.example.booktop.MVPContract;
import com.example.booktop.model.MVPModel;
import com.example.booktop.model.bookModels.Item;

import java.util.List;

public class MVPPresenter implements MVPContract.Presenter {
    MVPContract.Model model = new MVPModel();
    MVPContract.View view ;


    @Override
    public void attachView(MVPContract.View view) {
    this.view=view;
    model.attachPresenter(this);
    }

    @Override
    public void onGetBook(String bookName) {
        model.onGetBook(bookName);

    }

    @Override
    public void onResultReceived(List<Item> bookItems) {
    view.onResultReceived(bookItems);
    }
}
